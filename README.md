# Vägen_till_C

The goal is to contain all exercises from the book "Vägen till C". First digit represents 
the chapter and the last digit the exercise in that chapter. 281 Means chapter 2 exercise 1.

All programs are compiled with the flags. -Wall -pedantic -ansi.

For example:

```
$ cc 281.c -Wall -pedantic -ansi
$ ./a.out 
Rickard Bremer
Roligagatan 2
222 22 Ingenstans
```

