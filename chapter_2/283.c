#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int number, i;

    if (argc < 2)
    {
        printf("Usage: %s <number>\n", argv[0]);
        return 1;
    }
    else if (argv[1][0] - '0' == 0)
    {
        printf("Number must be higher then 0\n");
        return 1;
    }
    else if (!atoi(argv[1]))
    {
        printf("%s is not an integer\n", argv[1]);
        return 1;
    }
    
    number = atoi(argv[1]);

    printf(" i   i*i   i*i*i\n");
    printf("=== ===== =======\n");
    
    for  (i = 1; i <= number; i++)
    {
        printf(" %1d %4d %6d\n", i, i*i, i*i*i);
    }
    
    return 0;
}
