#include <stdio.h>

int main()
{
    float total_sum = 0.00, money = 0.01;
    int days = 1;

    while(total_sum <= 1000000)
    {
        total_sum += days * money;
        money = money * 2;
        days += 1;
    }

    printf("Days it took to reach 1000000 : %d\n", days);

    return 0;
}
