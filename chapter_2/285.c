#include <stdio.h>

#define DISCOUNT 0.9
#define MINIMUM_PAY 1000

int main()
{
    float number_of_items, cost_per_item, total_cost;

    printf("How many items do you want? ");
    scanf("%f", &number_of_items);
    printf("Cost per item? ");
    scanf("%f", &cost_per_item);

    total_cost = number_of_items * cost_per_item;
    
    if (total_cost >= MINIMUM_PAY)
    {
        total_cost = total_cost * DISCOUNT;
    }
    
    printf("Total cost: %.2f\n", total_cost);

    return 0; 
}
