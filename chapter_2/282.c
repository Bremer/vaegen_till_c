#include <stdio.h>

int main()
{
    float driven, one_year_ago, fuel, one_year, consumed;

    printf("How many swedish miles has the car driven? ");
    scanf("%f", &driven);
    printf("How many swedish miles had the car driven one year ago? ");
    scanf("%f", &one_year_ago);
    printf("How many liters of fuel have the car consumed the last year? ");
    scanf("%f", &fuel);

    one_year = driven - one_year_ago;
    consumed = fuel / one_year;

    printf("The car consumed an average of %g liters fuel per mile and have driven %g swedish miles.\n" , consumed, one_year);

    return 0;
}
